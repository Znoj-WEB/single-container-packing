### **Description**

Web app use 3dbinpacking.com to calculate if products fit the box or not. App is the same as the demo in [https://www.3dbinpacking.com/en/products/single-bin-packing](https://www.3dbinpacking.com/en/products/single-bin-packing)

---

### **Link**

serverd with Netlify:

-   [http://shipmonk.znoj.cz/](http://shipmonk.znoj.cz/)
-   [https://unruffled-noether-1397cc.netlify.app/](https://unruffled-noether-1397cc.netlify.app/)

---

### **Technologies**

TypeScript, React, Hooks, React Bootstrap, Redux

---

### **Year**

2020

---

### **Build & Deploy**

file .env has to contain REACT_APP_API_KEY (API KEY for REST API communication) and few others values for firebase deploy:

```
REACT_APP_API_KEY = "[put your value in here]"
REACT_APP_USERNAME = "[put your value in here]"
```

#### **Requirements for build**

-   [Node.js](https://nodejs.org/en/) 10 or newer
-   (optional) [yarn](https://yarnpkg.com/), alternative: `npm`

#### **Requirements for deploy with Netlify**

-   in the Site settings / Build & Deploy / Environment variables  
    put REACT_APP_API_KEY and REACT_APP_USERNAME variables from the `.env` file

---

### **Structure**

Project is based on `Create React App` TypeScript template.

#### **Conventions**

-   project specific component should be in folder `components`. When project will grow, more granularity will be needed - for example something could be divided by features or for example by region (group of pages)
-   if there will be more pages, every page should be in its own folder inside `pages` folder. Every logical component should be separated and if component appeares in other page too, it should be moved to `components`
-   everything conected directly with the application state should be inside `reducers` folder
-   everything conected directly with the saga should be inside `sagas` folder
-   if you have an enum, interface, type - put it into nearest folder where these 'types' are used into file `types.ts`
-   if you have a constant - put it into nearest folder into file `constants.ts`
-   if you have a general help function which could be put outside component, you should put it into nearest folder into file `utils.ts`

#### **Folders / Files**

| folders      | description                                                                       |
| ------------ | --------------------------------------------------------------------------------- |
| build        | folder with builded app                                                           |
| node_modules | folder with 3rd party packages downloaded with npm                                |
| public       | fodler with single page app config + icons                                        |
| README       | folder with files for README.md file (images, files or everything what is a BLOB) |
| src          | source code - eveything insteresting happens in here                              |

| files            | description                                                                    |
| ---------------- | ------------------------------------------------------------------------------ |
| .env             | untracked file with secret variables - you have to create one                  |
|                  | as mentioned in section 'Build & Deploy'                                       |
| .gitignore       | classic gitignore file - define what should and should not be tracked with GIT |
| .prettierrc.json | prettier rules for the project                                                 |
| package.json     | project settings, npm packages dependencies definition, scripts definitions    |
| README.md        | this README file                                                               |
| tsconfig.json    | TypeScript config file                                                         |
| yarn.lock        | detail package definition used by yarn                                         |

#### **src**

| folders    | description                                             |
| ---------- | ------------------------------------------------------- |
| components | reusable project specific component                     |
| reducers   | reducer(s) definition(s) - keep the app state in here   |
| sagas      | sagas definitions - keep the API handling stuff in here |

| files              | description                                                                    |
| ------------------ | ------------------------------------------------------------------------------ |
| actions.ts         | all actions are defined in here. If it will grow,                              |
|                    | file could be divided into multiple files and put into folder `actions`        |
| constants.ts       | global constants used accross the aplications -                                |
|                    | - also all 'MAGIC CONSTANTS' should be in here                                 |
| index.tsx          | starting point of this application                                             |
| react-app-env.d.ts | place where type definitions should appear - TECHNICAL DEBT                    |
| reportWebVitals.ts | report web vitals stuff                                                        |
| strings.ts         | all strings should appear in here for localization purposes                    |
| types.ts           | global types (interfaces, types, enums) definitions; if application will grow, |
|                    | file could be divided into multiple files and put into folder `types`          |
| utils.ts           | help functions used globally in the whole application                          |

#### **src/sagas**

-   critical point in here are missing unit/integration tests

| files    | description                                                                                         |
| -------- | --------------------------------------------------------------------------------------------------- |
| index.ts | every API endpoint (we have only one) has its own generator and use data from const.ts and utils.ts |
| utils.ts | help functions for the API communication - centralized error handling,                              |

### **Known bugs & TODO list**

-   redux-logger is enabled intentionaly, has to be removed before release
-   CSS are taken from original side, so it needs to be refactored (or better developped) for ShipMonks purposes
-   unit tests should be created (strating with `sagas` folder, then all `utils.ts` files
-   Big components and maybe `App.tsx` should be divided into more components
-   interfaces should be properly extracted from `components` + some of them should be renamed
-   confluence pages (or alternative) should be created from this README file to have proper documentation
-   errors from server should be analyzed and properly handled (starting point for that is in `sagas/utils.ts`)
-   if the app will grow, some of the components should become more general `UI component`

---

### **Screenshots**

![](./README/main.png)

![](./README/screenshot1.png)

![](./README/screenshot2.png)

![](./README/screenshot3.png)
