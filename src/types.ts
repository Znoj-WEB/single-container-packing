export interface IIsMobileStateProps {
    app: IApp;
}

export interface IBin {
    id: string;
    w: number;
    h: number;
    d: number;
    max_wg: number;
}

export interface IBinResponse extends IBin {
    used_space: number;
    weight: number;
    used_weight: number;
}

export type ItemColorSchemaType = 'default' | 'item' | 'random';

export interface IItem {
    id: string;
    w: number;
    h: number;
    d: number;
    wg: number;
    vr: 0 | 1;
    q: number;
    item_colors_schema?: ItemColorSchemaType;
    item_fill_color?: string;
    item_border_color?: string;
}

export interface ICoordinates {
    x1: number;
    y1: number;
    z1: number;
    x2: number;
    y2: number;
    z2: number;
}
export interface IItemResponse {
    id: string;
    image_separated: string;
    image_sbs: string;
    coordinates: ICoordinates;
}

export interface IPackParams {
    bins: IBin[];
    items: IItem[];
}

export interface IItemNotPacked {
    id: string;
    q: number;
}

export interface IBinPacked {
    bin_data: IBinResponse;
    image_complete: string;
    images_generation_time: number;
    packing_time: number;
    items: IItemResponse[];
    not_packed_items: IItemNotPacked[];
}

export type ErrorType = 'critical' | 'warning';
export interface IError {
    level: ErrorType;
    message: string;
}
export interface IPackResponseData {
    bins_packed: IBinPacked[];
    errors: IError[];
    status: 0 | 1;
    not_packed_items: any[];
    response_time: number;
}

export declare type NotificationType = 'success' | 'info' | 'error' | 'warning';

export interface IInStore {
    loading: boolean;
    error: string;
}
export interface IPackResponse extends IInStore {
    data: IPackResponseData;
}

export interface IApp {
    isMobile: boolean;
    bins: IBin[];
    items: IItem[];
}

export interface IDefaultState {
    app: IApp;
    packResponse: IPackResponse;
}

export interface IDefaultDispatchProps {
    updateIsMobile: (isMobile: boolean) => void;
    pack: (data: IPackParams) => void;
    binsUpdate: (data: IBin[]) => void;
    itemsUpdate: (data: IItem[]) => void;
}

export interface IDefaultProps extends IDefaultState, IDefaultDispatchProps {}
