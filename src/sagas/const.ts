export const BASE_URL = 'https://eu.api.3dbinpacking.com/packer/pack';
export const API_KEY = process.env.REACT_APP_API_KEY || '';
export const USERNAME = process.env.REACT_APP_USERNAME || '';
